# Вы можете импортировать файлы из
# модулей (директорий содержащих файл
# __init__.py)

# Если файлы импортированы в __init__.py
# то можно будет импортировать их из модуля
from functions import print_current_date_utc, sum_them_all

# Также можно импортировать любые объекты напрямую из файлов
from functions.calendar import SOME_OTHER_CONSTANT
from functions.calculator import SOME_CONSTANT

# Это абсолютный импорт (начинается не с точки - значит начинается из корневой директории проекта)
from config import CONFIG_VARIABLE

# Данная конструкция выполнится только если запустить файл напрямую
# (то есть не будет выполнена после импорта жтого файла)
if __name__ == '__main__':
    print_current_date_utc()
    print(SOME_CONSTANT, SOME_OTHER_CONSTANT, CONFIG_VARIABLE)
